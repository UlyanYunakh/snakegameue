// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeActor.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}


void AFood::Interacte()
{
	ASnakeActor* Snake = (ASnakeActor*)UGameplayStatics::GetActorOfClass(GetWorld(), ASnakeActor::StaticClass());
	if (IsValid(Snake))
	{
		Snake->AddSnakePart();
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, TEXT("Snake ate something delicious"));
		this->Destroy();
	}
}

