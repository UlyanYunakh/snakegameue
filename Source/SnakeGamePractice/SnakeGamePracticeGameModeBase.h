// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGamePracticeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAMEPRACTICE_API ASnakeGamePracticeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
