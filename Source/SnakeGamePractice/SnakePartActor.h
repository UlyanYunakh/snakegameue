// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakePartActor.generated.h"

UCLASS()
class SNAKEGAMEPRACTICE_API ASnakePartActor : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakePartActor();

	UPROPERTY()
	class UStaticMeshComponent* cubeMeshComponent;

protected:
	UFUNCTION()
	void Interacte() override;
};
