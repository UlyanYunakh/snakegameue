// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameField.generated.h"

UCLASS()
class SNAKEGAMEPRACTICE_API AGameField : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameField();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable)
	void GenerateSpawnPoints();

	UFUNCTION()
	void SpawnFood();

	UFUNCTION()
	void RespawnFood(AActor* Other);

public:
	UPROPERTY(EditAnywhere)
	int X;

	UPROPERTY(EditAnywhere)
	int Y;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class AFood> FoodClass;

	UPROPERTY()
	TArray<class ASpawnPoint*> SpawnPoints;

private:
	UPROPERTY()
	int DefaultStepLenght = 130;

	UPROPERTY()
	class ASnakeActor* Snake;

	UPROPERTY()
	class AFood* Food;

	UPROPERTY()
	FTimerHandle TimeHandle;
};
