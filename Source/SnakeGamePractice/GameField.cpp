// Fill out your copyright notice in the Description page of Project Settings.


#include "GameField.h"
#include "SpawnPoint.h"
#include "Components/ChildActorComponent.h"
#include "SnakePawn.h"
#include "SnakeActor.h"
#include "SnakePartActor.h"
#include "Food.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
AGameField::AGameField()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AGameField::BeginPlay()
{
	Super::BeginPlay();

	UWorld* Level = GetWorld();

	if (IsValid(Level) && X > 0 && Y > 0 && !SpawnPoints.IsEmpty())
	{
		FVector ActorLocation = SpawnPoints[(SpawnPoints.Num() - 1)  / 2]->GetActorLocation();
		Snake = Level->SpawnActor<ASnakeActor>(ActorLocation, GetActorRotation());

		ASnakePawn* Pawn = Level->SpawnActor<ASnakePawn>();

		APlayerController* Controller = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		Controller->Possess(Pawn);

		SpawnFood();
	}
}

void AGameField::GenerateSpawnPoints()
{
	if (RootComponent->GetName() != TEXT("RootComp"))
	{
		UPrimitiveComponent* RootComp = NewObject<UPrimitiveComponent>(this, TEXT("RootComp"));
		RootComp->RegisterComponent();
		RootComp->SetWorldTransform(GetActorTransform());
		SetRootComponent(RootComp);
	}

	if (!SpawnPoints.IsEmpty())
	{
		TArray<USceneComponent*> ChildrenComp;
		RootComponent->GetChildrenComponents(true, ChildrenComp);
		for (auto child : ChildrenComp)
		{
			child->UnregisterComponent();
		}
		SpawnPoints.Empty();
	}

	for (int i = 0; i < X; i++)
	{
		for (int j = 0; j < Y; j++)
		{
			UChildActorComponent* ChildActorComp = NewObject<UChildActorComponent>(this);
			ChildActorComp->SetChildActorClass(ASpawnPoint::StaticClass());
			ChildActorComp->RegisterComponent();
			ChildActorComp->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
			ChildActorComp->SetRelativeLocation(FVector(DefaultStepLenght * i, DefaultStepLenght * j, 0));

			ASpawnPoint* SpawnPoint = Cast<ASpawnPoint>(ChildActorComp->GetChildActor());
			if (SpawnPoint)
			{
				SpawnPoints.Add(SpawnPoint);
			}
		}
	}
}

void AGameField::SpawnFood()
{
	TArray<ASpawnPoint*> AvailablePoints = SpawnPoints.FilterByPredicate([](ASpawnPoint* Point) {
		return Point->IsFree;
	});

	if (AvailablePoints.IsEmpty())
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, TEXT("Snake is full"));
		return;
	}


	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::FromInt(AvailablePoints.Num()));

	ASpawnPoint* Point = AvailablePoints[FMath::RandRange(0, AvailablePoints.Num() - 1)];

	UWorld* Level = GetWorld();

	if (IsValid(Level) && X > 0 && Y > 0)
	{
		Food = Level->SpawnActor<AFood>(FoodClass, Point->GetActorLocation(), Point->GetActorRotation());
		Food->OnDestroyed.AddDynamic(this, &AGameField::RespawnFood);
	}
}

void AGameField::RespawnFood(AActor* Other)
{
	Food->OnDestroyed.RemoveDynamic(this, &AGameField::RespawnFood);
	GetWorldTimerManager().SetTimer(TimeHandle, this, &AGameField::SpawnFood, .5f, false, .5f);
}

