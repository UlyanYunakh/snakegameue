// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "SnakeActor.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AWall::Interacte()
{
	ASnakeActor* Snake = (ASnakeActor*)UGameplayStatics::GetActorOfClass(GetWorld(), ASnakeActor::StaticClass());
	if (IsValid(Snake))
	{
		Snake->DestroySnakeParts();
		Snake->Destroy();
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Snake hit the wall and died"));
	}
}

