// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePartActor.h"
#include "SnakeActor.h"
#include <Kismet/GameplayStatics.h>

// Sets default values
ASnakePartActor::ASnakePartActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	cubeMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cube"));

	UStaticMesh* cubeMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'")).Object;
	cubeMeshComponent->SetStaticMesh(cubeMesh);
	cubeMeshComponent->SetGenerateOverlapEvents(true);
	cubeMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	RootComponent = cubeMeshComponent;
}

void ASnakePartActor::Interacte()
{
	ASnakeActor* Snake = (ASnakeActor*)UGameplayStatics::GetActorOfClass(GetWorld(), ASnakeActor::StaticClass());
	if (IsValid(Snake))
	{
		Snake->DestroySnakeParts();
		Snake->Destroy();
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Snake ate itself"));
	}
}
