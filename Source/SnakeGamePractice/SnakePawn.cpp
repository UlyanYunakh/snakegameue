// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePawn.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeActor.h"

// Sets default values
ASnakePawn::ASnakePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASnakePawn::BeginPlay()
{
	Super::BeginPlay();

	Snake = (ASnakeActor*) UGameplayStatics::GetActorOfClass(GetWorld(), ASnakeActor::StaticClass());
}

// Called every frame
void ASnakePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASnakePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis("Up", this, &ASnakePawn::MoveUp);
	InputComponent->BindAxis("Right", this, &ASnakePawn::MoveRight);
}

void ASnakePawn::MoveUp(float value)
{
	if (value != 0)
	{
		if (IsValid(Snake))
		{
			Snake->UpdateMoveDirection(value > 0 ? FVector(0, 1, 0) : FVector(0, -1, 0));
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Snake died"));
		}
	}
}


void ASnakePawn::MoveRight(float value)
{
	if (value != 0) 
	{
		if (IsValid(Snake))
		{
			Snake->UpdateMoveDirection(value > 0 ? FVector(-1, 0, 0) : FVector(1, 0, 0));
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Snake died"));
		}
	}
}

