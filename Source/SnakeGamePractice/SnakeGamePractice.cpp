// Copyright Epic Games, Inc. All Rights Reserved.

#include "SnakeGamePractice.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SnakeGamePractice, "SnakeGamePractice" );
