// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActor.h"
#include "SnakePartActor.h"
#include "SnakeHeadComponent.h"

// Sets default values
ASnakeActor::ASnakeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MoveDirection = FVector::ForwardVector;

	auto root = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRoot"));
	SetRootComponent(root);
}

// Called when the game starts or when spawned
void ASnakeActor::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(0.5);

	for (int i = 0; i < SNAKE_DEFAULT_PARTS_NUMBER; i++)
	{
		auto newPart = GetWorld()->SpawnActor<ASnakePartActor>();
		newPart->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
		newPart->SetActorLocation(GetActorLocation() - MoveDirection * DistanceBetweenParts * i);
		Parts.AddUnique(newPart);
	}

	ASnakePartActor* head = Parts[0];
	if (IsValid(head))
	{
		USnakeHeadComponent* comp = NewObject<USnakeHeadComponent>(head, TEXT("Head Component"));
		comp->RegisterComponent();
	}
}

// Called every frame
void ASnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeActor::UpdateMoveDirection(FVector NewDirection)
{
	if (FVector::DotProduct(LastDirection, NewDirection) >= 0)
	{
		MoveDirection = NewDirection;
	}
}

void ASnakeActor::Move()
{
	auto head = Parts[0];
	auto headPrevLocation = head->GetActorLocation();
	Tail = Parts.Last()->GetActorLocation();

	head->AddActorWorldOffset(MoveDirection * DistanceBetweenParts);

	for (int i = Parts.Num() - 1; i > 0; i--)
	{
		auto part = Parts[i];
		FVector newLocation = i == 1 ? headPrevLocation : Parts[i - 1]->GetActorLocation();
		part->SetActorLocation(newLocation);
	}

	LastDirection = MoveDirection;
}

void ASnakeActor::DestroySnakeParts()
{
	for (auto item : Parts)
	{
		item->Destroy();
	}
}

void ASnakeActor::AddSnakePart()
{
	auto newPart = GetWorld()->SpawnActor<ASnakePartActor>();
	newPart->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	newPart->SetActorLocation(Tail);
	Parts.AddUnique(newPart);
}
