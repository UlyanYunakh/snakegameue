// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeActor.generated.h"

class ASnakePartActor;

UCLASS()
class SNAKEGAMEPRACTICE_API ASnakeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeActor();

private:
	UPROPERTY()
	int SNAKE_DEFAULT_PARTS_NUMBER = 3;

	UPROPERTY()
	int DistanceBetweenParts = 130;

	UPROPERTY()
	TArray<ASnakePartActor*> Parts;

	UPROPERTY()
	FVector MoveDirection;

	UPROPERTY()
	FVector LastDirection;

	UPROPERTY()
	FVector Tail;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void Move();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void DestroySnakeParts();

	UFUNCTION()
	void AddSnakePart();

	UFUNCTION()
	virtual void UpdateMoveDirection(FVector NewDirection);

};
