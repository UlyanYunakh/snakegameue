// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeHeadComponent.h"
#include "SnakePartActor.h"

// Sets default values for this component's properties
USnakeHeadComponent::USnakeHeadComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void USnakeHeadComponent::BeginPlay()
{
	Super::BeginPlay();

	ASnakePartActor* head = (ASnakePartActor*)GetOwner();

	if (IsValid(head))
	{
		head->cubeMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &USnakeHeadComponent::OnOverlap);
	}
}

void USnakeHeadComponent::OnOverlap(UPrimitiveComponent* comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto t = Cast<IInteractable>(OtherActor);
	if (t)
	{
		t->Interacte();
	}
}

