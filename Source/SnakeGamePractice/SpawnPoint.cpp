// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnPoint.h"
#include "Components/BoxComponent.h"

// Sets default values
ASpawnPoint::ASpawnPoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(FName("BoxCollision"));
	BoxCollision->SetLineThickness(2);
	BoxCollision->SetHiddenInGame(false);
	SetRootComponent(BoxCollision);
}

void ASpawnPoint::BeginPlay()
{
	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &ASpawnPoint::OnOverlapBegin);
	BoxCollision->OnComponentEndOverlap.AddDynamic(this, &ASpawnPoint::OnOverlapEnd);
}

void ASpawnPoint::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	IsFree = false;
}

void ASpawnPoint::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	GetWorldTimerManager().SetTimer(TimeHandle, this, &ASpawnPoint::SetFree, .5f, false, .5f);
}

void ASpawnPoint::SetFree()
{
	TArray<AActor*> Overlaping;
	BoxCollision->GetOverlappingActors(Overlaping);
	if (Overlaping.IsEmpty())
	{
		IsFree = true;
	}
}
